<?php

require("app/Config.php");
require("vendor/autoload.php");

use Core\Router;
use Helpers\Session;

error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', 1);

date_default_timezone_set(TIMEZONE);

Session::init();

Routes::load();
Router::dispatch($_SERVER["REQUEST_URI"] ?: "/", $_SERVER['REQUEST_METHOD']);