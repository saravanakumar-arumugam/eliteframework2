<?php

use Core\Router;

class Routes
{
    public static function load()
    {
        Router::$basePath = DIR;
        Router::$defaultController = "Home";
        Router::$defaultAction = "index";
        Router::$routes = [
            // Controllers with namespace
            'Controllers'=> [
                '/[{controller}]', '/{controller}[/{action}]', '/{controller}/{action}[/{param}]'
            ]
        ];
        Router::$text404 = self::show404();
    }

    public static function show404()
    {
        $constant = 'constant';
        return <<<HTML
<html>
    <head>
        <title>404 Page Not Found</title>
        <style>
            body{
                margin:0;
                padding:30px;
                font:12px/1.5 Helvetica,Arial,Verdana,sans-serif;
            }
            h1{
                margin:0;
                font-size:48px;
                font-weight:normal;
                line-height:48px;
            }
            h3{
                margin:0;
                font-size:32px;
                font-weight:normal;
                line-height:48px;
            }
            strong{
                display:inline-block;
                width:65px;
            }
        </style>
    </head>
    <body>
        <h1>404</h1>
        <h3>Page Not Found</h3>
        <p>
            The page you are looking for could not be found. Check the address bar
            to ensure your URL is spelled correctly. If all else fails, you can
            visit our home page at the link below.
        </p>
        <a href="{$constant('DIR')}/">Visit the Home Page</a>
    </body>
</html>
HTML;
    }
}