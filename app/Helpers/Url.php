<?php namespace Helpers;

class Url
{
    public static function redirect($url, $fullpath=false)
    {
        if (!$fullpath) {
            $url = str_replace('//', '/', DIR . $url);
        }
        header('Location: ' . $url);
        exit;
    }
}