<?php namespace Helpers;

class Csrf
{
    public static function makeToken()
    {
        return md5(rand(1000, 9999));
    }

    public static function verifyToken($token, $hash)
    {
        return $token == $hash;
    }
}