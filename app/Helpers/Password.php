<?php namespace Helpers;

class Password
{
    public static function make($password, $hash = false)
    {
        $salt = ($hash) ? $hash : '$2y$05$' . substr(md5(rand(1, 9999)), 0, 25) . '$';
        return crypt($password, $salt);
    }

    public static function verify($password, $hash)
    {
        return self::make($password, $hash) == $hash;
    }
}