<?php namespace Helpers;

class Session
{
    public static function init()
    {
        session_start();
    }

    public static function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public static function get($key)
    {
        return $_SESSION[$key];
    }

    public static function pull($key)
    {
        $result = $_SESSION[$key];
        unset($_SESSION[$key]);
        return $result;
    }

    public static function destroy()
    {
        session_destroy();
    }
}