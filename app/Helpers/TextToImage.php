<?php namespace Helpers;

class TextToImage
{
    public static function base64($text, $width, $height)
    {
        // Create image
        $im = imagecreatetruecolor($width, $height);

        // Create colors
        $white = imagecolorallocate($im, 255, 255, 255);
        $grey = imagecolorallocate($im, 180, 180, 180);
        $black = imagecolorallocate($im, 0, 0, 0);
        imagefilledrectangle($im, 0, 0, ($width - 1), ($height - 1), $white);
        
        // Insert string with shades
        imagestring($im, 5, 2, 1, $text, $grey);
        imagestring($im, 5, 0, 0, $text, $black);

        ob_start();
        imagepng($im);
        $image_data = base64_encode(ob_get_contents());
        ob_end_clean();
        imagedestroy($im);
        return $image_data;
    }
}
