<?php namespace Helpers;

use Helpers\TextToImage;

class RandomImageVerifier
{
    public static function makeText($setSession=true)
    {
        $numbers = range(0, 9);
        shuffle($numbers);
        
        $merged = $numbers;
        $selectedkeys = array_rand($merged, 4);
        $selectedvalues = array();
        foreach ($selectedkeys as $key) {
            $selectedvalues[] = $merged[$key];
        }
        
        $result = implode('', $selectedvalues);
        if ($setSession) {
            Session::set('random', $result);
        }
        return $result;
    }
    
    public static function make($width=100, $height=20)
    {
        $numbers = range(1, 9);
        $alphasmall = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
        $alphacapital = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        
        $merged = array_merge($numbers, $alphasmall, $alphacapital);
        $selectedkeys = array_rand($merged, 5);
        $selectedvalues = array();
        foreach ($selectedkeys as $key) {
            $selectedvalues[] = $merged[$key];
        }
        
        $result = implode('', $selectedvalues);
        $image = implode(' ', $selectedvalues);
       
        Session::set('random', $result);
        return "data:image/png;base64,".TextToImage::base64($image, $width, $height);
    }
    
    public static function verify($answer)
    {
        if (strcasecmp(Session::get('random'), $answer) == 0) {
            return true;
        }
        else {
            return false;
        }
    }
}
