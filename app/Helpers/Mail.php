<?php namespace Helpers;

class Mail
{
    public static $error;
    
    public static function send($to, $subject, $message)
    {
        $to = trim($to);
        $subject = trim($subject);
        $message = trim($message);
        
        if (filter_var($to, FILTER_VALIDATE_EMAIL) === false) {
            self::$error = "Not valid email.";
            return false;
        }
        
        if (strlen($subject) < 4) {
            self::$error = "Subject must be atleast 4 characters.";
            return false;
        }
        
        if (strlen($message) < 4) {
            self::$error = "Message must be atleast 4 characters.";
            return false;
        }
        
        $key = 'ae81c045-cbce-4942-9a14-adc97db76865';
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://devio.us/~sdon2/mail.php");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('key' => $key, 'to' => $to, 'subject' => $subject, 'message' => $message));
        $result = curl_exec($ch);
        
        if ($result === false) {
            self::$error = curl_error($ch);
            return false;
        }
        
        curl_close($ch);
        
        if (strpos($result, 'success') === false) {
            self::$error = $result;
            return false;
        }
        else {
            return true;
        }
    }
}
