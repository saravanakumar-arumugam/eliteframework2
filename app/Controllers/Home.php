<?php namespace Controllers;

use Core\Controller;
use Helpers\Url;

class Home extends Controller
{
    public function getIndex()
    {
        echo "<h1>Hello, World!</h1>";
    }
}