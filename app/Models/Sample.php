<?php namespace Models;

use Core\Model;

class Sample extends Model
{
    public function __construct()
    {
        parent::__construct("sample");
    }
}