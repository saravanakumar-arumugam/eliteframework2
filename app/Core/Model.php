<?php namespace Core;

use Core\Database;

abstract class Model
{
    private $db;
    protected $table;
    public $tableName;

    public function __construct($table)
    {
        $this->db = new Database(DB_HOST, DB_NAME, DB_USER, DB_PASS);
        $this->table = $this->db->from($table);
        $this->tableName = $table;
    }

    public function getAll()
    {
        return $this->table->fetchAll();
    }

    public function getById($id)
    {
        return $this->table->where("id", $id)->fetch();
    }

    public function insert($data)
    {
        return $this->insertInto($this->tableName, $data)->execute();
    }

    public function update($id, $data)
    {
        return $this->update($this->tableName)->set($data)->where("id", $id)->execute();
    }

    public function delete($id)
    {
        return $this->deleteFrom($this->tableName)->where("id", $id)->execute();
    }
}