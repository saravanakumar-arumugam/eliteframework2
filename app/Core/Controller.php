<?php namespace Core;

abstract class Controller
{
    private $view;

    public function __construct()
    {
        $this->view = new View();
    }

    public function __before($param)
    {
        return true;
    }

    public function render($template = "", $data = "")
    {
        $this->view->render($template, $data);
    }

    public function __after($param = null)
    {
        return true;
    }
}