<?php namespace Core;

class Router
{
    public static $basePath = "";
    public static $defaultController;
    public static $defaultAction;
    public static $routes = [];
    public static $text404 = "Not found.";

    public static function dispatch($uri = "", $method = "GET")
    {
        // Strip root directory
        $uri = substr($uri, strlen(self::$basePath));

        // Strip query string (?foo=bar) and decode URI
        if (false !== $pos = strpos($uri, '?')) {
            $uri = substr($uri, 0, $pos);
        }
        $uri = rawurldecode($uri);

        // Use fast-route router
        $dispatcher = \FastRoute\simpleDispatcher(function(\FastRoute\RouteCollector $r) use ($method) {
            // Loop through all routes
            foreach (self::$routes as $ns=>$sroutes) {
                foreach ($sroutes as $route) {
                    try {
                        $r->addRoute(strtoupper($method), $route, $ns);
                    } catch (\Exception $e) {
                        // Ignore all exceptions
                    }
                }
            }
        });
        $result = $dispatcher->dispatch($method, $uri);

        // var_dump($result);

        // No route matches found
        if ($result[0] == 0) {
            self::show404();
        }

        // Extract namespace, controller, action and param from route
        $ns = $result[1];
        $routeComponents = $result[2];
        $controller = isset($routeComponents["controller"]) ? $routeComponents["controller"] : self::$defaultController;
        $action = isset($routeComponents["action"]) ? $routeComponents["action"] : self::$defaultAction;
        $param = isset($routeComponents["param"]) ? $routeComponents["param"] : false;

        // Add base namespace and merge method with action
        $controller = $ns."\\".ucfirst($controller);
        $action = strtolower($method).ucfirst($action);

        // Check whether the controller exists
        if (class_exists($controller) && method_exists($controller, $action)) {
            if (call_user_func_array(array(new $controller, "__before"), [$param])) {
                call_user_func_array(array(new $controller, $action), [$param]);
                call_user_func_array(array(new $controller, "__after"), [$param]);
                return;
            }
        }
        else {
            // Controller doesn't exist
            // self::$text404 = "Not found. $controller, $action";
            self::show404();
        }
    }

    private static function show404()
    {
        header($_SERVER['SERVER_PROTOCOL']." 404 Not Found");
        die(self::$text404);
    }
}