<?php namespace Core;

use Helpers\Session;

class View
{
    private $twig;

    public function __construct()
    {
        \Twig_Autoloader::register();
        $loader = new \Twig_Loader_Filesystem(array(TEMPLATE_PATH));
        $params = array(
            'cache' => 'temp/cache',
            'auto_reload' => true,
            'autoescape' => false
        );
        $this->twig = new \Twig_Environment($loader, $params);
    }

    public function render($path, $data = false)
    {
        $template = $this->twig->loadTemplate("$path.twig");
        echo $template->render(array(
            "data"      => $data,
            "messages"   => Session::pull('messages'),
            "errors"     => Session::pull('errors'),
            "DIR"       => DIR,
            "userdata"  => Session::get('userdata'),
            "GET"       => $_GET,
            "POST"      => $_POST,
        ));
    }
}