<?php namespace Core;

use FluentPDO;

class Database extends FluentPDO
{
    public function __construct($host, $db, $user, $pass)
    {
        $pdo = new \PDO("mysql:host=$host;dbname=$db", $user, $pass);
        parent::__construct($pdo);
    }
}