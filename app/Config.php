<?php

// Development mode or production mode
define("MODE", "development");

// Sitewide title
define("SITE_TITLE", "Elite Framework 2");

// Root installation directory without trialing slashes.
// Empty for root directory
define("DIR", "");

// Twig template path
define("TEMPLATE_PATH", "app/Views");

// Database info
define('DB_HOST', 'localhost');
define('DB_NAME', 'db');
define('DB_USER', 'root');
define('DB_PASS', '');
define('TABLE_PREFIX', '');

// Local timezone
define("TIMEZONE", "Asia/Kolkata");